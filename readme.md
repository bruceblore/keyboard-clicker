# Keyboard clicker

Click on certain coordinates on the screen based on keyboard input. Helpful for classroom quiz games like Kahoot because it prevents you from needing to look at your screen and move the cursor around, allowing you to have your answer in as soon as you think of it.

## Getting Started

### Prerequisites

Prerequisites

```
pip install pyautogui pyxhook pyqt5
```

### Installing

1. Clone the repo
2. (Optional) Add a symlink to main.py to your $PATH
3. (Optional) Use your GUI's main menu editor to add the program to your app menu
4. (Optional) Set up a cronjob to automatically pull from my repo every so often in case of updates

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Bruce Blore** - *Everything* - [0100001001000010](https://gitlab.com/0100001001000010)
